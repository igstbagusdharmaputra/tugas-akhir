# algoritma seleksi fitur information gain
from csv import reader
from math import log
from prettytable import PrettyTable
# Load a CSV file
def load_csv(filename):
    dataset = list()
    with open(filename, 'r') as file:
        csv_reader = reader(file)
        for row in csv_reader:
            if not row:
                continue
            dataset.append(row)
    return dataset

def total(data,dataset):
    col = len(data)-1
    kelas = data[col]

    result = [[[0 for k in range(len(kelas))] for j in range(len(data[i]))] for i in range(len(data)-1)]
    for i in range(len(kelas)): #kelas YA dan Tidak
        for j in range(len(dataset[0])-1): #jumlah kolom pada dataset
            for k in range(len(data[j])): #jumlah ['>40', '<=30', '31…40'], ['RENDAH', 'SEDANG', 'TINGGI'], ['BUKAN', 'YA'], ['BIASA', 'SANGAT BAGUS'], ['TIDAK', 'YA']
                count = 0
                for l in range(len(dataset)): #jumlah row pada dataset
                    if dataset[l][j] == data[j][k] and kelas[i] == dataset[l][col]:
                        count+=1
                result[j][k][i] = count
    return result
def count_class(data,dataset):
    result = []
    col = len(dataset[0])-1
    row = len(dataset)
    for i in range(len(data)):
        total=0
        for j in range(row):
            if dataset[j][col] == data[i]:
                total+=1
        result.append(total)
    return  result


def entropy(pi):
    total = 0
    for p in pi:
        p = p / sum(pi)
        if p != 0:
            total += p * log(p, 2)
        else:
            total += 0
    total *= -1
    return total

def gain(class_dataset,atribut_dataset):
    total = 0
    for v in atribut_dataset:
        total+= sum(v) / sum(class_dataset) * entropy(v)

    gain = entropy(class_dataset) - total
    return gain


# fields = ['Umur','Penghasilan','Mahasiswa','Peringkat Kredit']
dataset = load_csv('resulttrain.csv')
kolom = len(dataset[0])-1
uniqueValueAtribut = [list(set(row)) for row in zip(*dataset)] #tips keren unik value dapatkan nilai atribut
resultTable = total(uniqueValueAtribut,dataset) #nilai total masing-masing record
resultClass = count_class(uniqueValueAtribut[len(uniqueValueAtribut)-1],dataset) #total nilai pada class
result = [[0 for k in range(0)] for j in range(kolom)] #menyimpan hasil
temp = [[0 for k in range(0)] for j in range(kolom)] #penyimpan sementara nilai pada setiap atribut

for i in range(len(resultTable)):
    for j in range(len(resultTable[i])):
        temp[i].append(resultTable[i][j])
    result[i].append(gain(resultClass,temp[i]))
    result[i].append(i)
result.sort(key=lambda x: x[0]) #sort data
result.reverse()
# tampilan dengan table
data = ['duration','protocol_type','service','flag','src_bytes','dst_bytes','land','wrong_fragment','urgent','hot','num_failed_logins','logged_in','num_compromised','root_shell','su_attempted','num_root','num_file_creations','num_shells','num_access_files','num_outbound_cmds','is_host_login','is_guest_login','count','srv_count','serror_rate','srv_serror_rate','rerror_rate','srv_rerror_rate','same_srv_rate',
        'diff_srv_rate','srv_diff_host_rate','dst_host_count','dst_host_srv_count','dst_host_same_srv_rate','dst_host_diff_srv_rate',
        'dst_host_same_src_port_rate','dst_host_srv_diff_host_rate','dst_host_serror_rate','dst_host_srv_serror_rate','dst_host_rerror_rate',
        'dst_host_srv_rerror_rate'
        ]
x = PrettyTable()
x.field_names = ["Atribut", "Information Gain",'Atribut Name']
for i in range(kolom):
    for j in range(kolom):
        if result[j][1] == i:
            # x.add_row([fields[i],result[i][0]])
            x.add_row([result[i][1]+1 ,result[i][0], data[result[i][1]]])
print(x)
