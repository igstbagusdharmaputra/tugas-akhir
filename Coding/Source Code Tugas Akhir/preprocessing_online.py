import csv
import pandas as pd
new_data = [] # dataset baru
check_tipe_data = [] #check dataset
def load_csv(filename):
    with open(filename) as csvfile:
        reader = csv.reader(csvfile)
        for data in reader:
            new_data.append(data)
    return new_data
def convert_string_to_float_and_integer():
    #integer = 0 float = 1
    row = len(new_data)
    col = len(new_data[0])
    #konversi tipe data
    for i in range(row):
       for j in range(col):
           if j!=1 and j!=2 and j!=3 and j!=11 and j!=20 and j!=21 and j!=6:
               if j<=23 or j==31 or j==32: #dari spreadsheets
                   new_data[i][j] = int(new_data[i][j])
               else:
                   new_data[i][j] = float(new_data[i][j])
    #check_tipe_data
    for i in range(col):
        check = new_data[0][i]
        if isinstance(check,int):
            check_tipe_data.append(0)
        elif isinstance(check,float):
            check_tipe_data.append(1)
        else:
            check_tipe_data.append('')
    return new_data


def discretization():
    data_set_transpose = [[new_data[j][i] for j in range(len(new_data))] for i in range(len(new_data[0]))]
    labels = ['low', 'medium', 'high']
    for i in range(len(data_set_transpose)):
        if check_tipe_data[i] == 1 or check_tipe_data[i]==0: #check_tipe_data
            data = pd.cut(data_set_transpose[i], 3, labels=labels)
            for j in range(len(data_set_transpose[0])):
                new_data[j][i] = data[j]
    return new_data

print('Preprocessing Data Online')

dataset = load_csv('data_online.csv')
data = convert_string_to_float_and_integer()
result = discretization()
kolom = ['duration', 'protocol_type', 'service', 'flag', 'src_bytes', 'dst_bytes', 'land', 'wrong_fragment',
         'urgent', 'hot', 'num_failed_logins',
         'logged_in', 'num_compromised', 'root_shell', 'su_attempted', 'num_root', 'num_file_creations',
         'num_shells', 'num_access_files', 'num_outbound_cmds',
         'is_host_login', 'is_guest_login', 'count', 'srv_count', 'serror_rate', 'srv_serror_rate', 'rerror_rate',
         'srv_rerror_rate', 'same_srv_rate',
         'diff_srv_rate', 'srv_diff_host_rate', 'dst_host_count', 'dst_host_srv_count', 'dst_host_same_srv_rate',
         'dst_host_diff_srv_rate',
         'dst_host_same_src_port_rate', 'dst_host_srv_diff_host_rate', 'dst_host_serror_rate',
         'dst_host_srv_serror_rate', 'dst_host_rerror_rate',
         'dst_host_srv_rerror_rate']
res = [ele[:] for ele in result]
f = open('resulttest_online.csv', 'w')
f.write(','.join([str(x) for x in kolom]) + '\n')
for item in res:
    f.write(','.join([str(x) for x in item]) + '\n')
f.close()




