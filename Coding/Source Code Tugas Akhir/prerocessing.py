import csv
from sklearn.datasets import load_iris
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
new_data = []


with open('KDDTrain+.csv') as csvfile:
    reader = csv.reader(csvfile)
    for data in reader:
        if data[41] == "normal":
            new_data.append(data)
        else:
            data[41] = "anomaly"
            new_data.append(data)
# Load iris data
iris_dataset = load_iris()

# Create features and target
X = iris_dataset.data
y = iris_dataset.target

# Convert to categorical data by converting data to integers
X = X.astype(int)
print(X)
chi2_features = SelectKBest(chi2, k=2)
X_kbest_features = chi2_features.fit_transform(X, y)

# Reduced features
print('Original feature number:', X.shape[1])
print('Reduced feature number:', X_kbest.shape[1])
#
# def load_csv(filename):
#
#     with open(filename) as csvfile:
#         reader = csv.reader(csvfile)
#         for data in reader:
#             if data[41] == "normal":
#                 new_data.append(data)
#             else:
#                 data[41] = "anomaly"
#                 new_data.append(data)
#     return new_data
# def datane():
#     set = []
#     for i in new_data:
#         set.append(int(i[19]))
#     df = pd.DataFrame(set)
#     min_max_scaler = preprocessing.MinMaxScaler()
#     X_train_minmax = min_max_scaler.fit_transform(df)
#     df_normalized = pd.DataFrame(X_train_minmax)
#     print(df_normalized)


# def unique(list1):
#     unique_list = []
#     for x in list1:
#         if x not in unique_list:
#             unique_list.append(x)
#     return unique_list
# def information_gain(dataset):
#     #total kolom dikurangi 2 karena tidak termasuk atribut
#     total_kolom = (len(dataset[0])-2)
#     #total baris dari data_train
#     total_baris = len(dataset)
#     #matrix transpose
#     data_set_transpose = [[dataset[j][i] for j in range(total_baris)] for i in range(total_kolom)]
#     cek = []
#     # 0 = numeric
#     # 1 = nominal
#     #mengubah data string menjadi float pada atribut numeric
#     for i in range(total_kolom):
#         for j in range(total_baris):
#             if  i == 1 or i == 2 or i == 3 or i == 6 or i == 11 or i == 20 or i == 21:
#                 continue
#             else:
#                 data_set_transpose[i][j] = float(data_set_transpose[i][j])
#         if i == 1 or i == 2 or i == 3 or i == 6 or i == 11 or i == 20 or i == 21:
#             cek.append(1)
#         else:
#             cek.append(0)
#
#     #mencari rata-rata pada atribut numeric
#     mean = []
#     data_unique_value = [[] for i in range(total_kolom)]
#     row = len(data_set_transpose[0])
#     col = len(data_set_transpose)
#     for i in range(col):
#         total = 0.0
#         for j in range(row):
#             if cek[i]==0:
#                 total += data_set_transpose[i][j]
#         total/=row
#         mean.append(round(total,3))
#     print(mean)
#     #mencari unique value pada atribut nominal
#     for i in  range(total_kolom):
#         if cek[i]==1:
#             data = unique(data_set_transpose[i])
#             data_unique_value[i].append(data)
#
#     label = [[] for i in range(total_kolom)]
#     for i in range(2): # total inisialisasi variable < dan >=
#         for j in range(total_kolom):
#             counta = 0  # hitung normal
#             countb = 0  # hitung anomaly
#             for k in range(total_baris):
#                 if cek[j]==0:
#                     if i==0: #kategori lebih kecil dari mean tiap atribut
#                         if data_set_transpose[j][k]<=mean[j]:
#                             if dataset[k][41] == "normal":
#                                 counta+=1
#                             elif dataset[k][41] == "anomaly":
#                                 countb+=1
#                     if i==1: #kategori lebih besar sama dengan dari mean tiap atribut
#                         if data_set_transpose[j][k]>=mean[j]:
#                             if dataset[k][41] == "normal":
#                                 counta+=1
#                             elif dataset[k][41] == "anomaly":
#                                 countb+=1
#             label[j].append(counta)
#             label[j].append(countb)
#
#     t=0
#     for i in label:
#         t+= 1
#         print(t)
#         print(i)


# filename ='KDDTrain+.csv'
# dataset = load_csv(filename)
# datane()
# selection_feature = information_gain(dataset)
# print(selection_feature)
# deklarasi 2 dimenasi
# test = []
# for i in range(5):
#     test.append([])


