count --> count_sec
srv_count --> srv_count_sec
serror_rate --> serror_rate_sec
srv_serror_rate --> srv_serror_rate_sec
rerror_rate --> rerror_rate_sec
srv_rerror_rate --> srv_rerror_rate_sec
same_srv_rate --> same_srv_rate_sec
diff_srv_rate --> diff_srv_rate_sec
srv_diff_host_rate --> srv_diff_host_rate_sec
dst_host_count --> count_100
dst_host_srv_count --> srv_count_100
dst_host_same_srv_rate --> same_srv_rate_100
dst_host_diff_srv_rate --> diff_srv_rate_100
dst_host_same_src_port_rate --> same_src_port_rate_100
dst_host_ srv_diff_host_rate --> srv_diff_host_rate_100
dst_host_ serror_rate --> serror_rate_100
dst_host_ srv_serror_rate --> srv_serror_rate_100
dst_host_ rerror_rate --> rerror_rate_100
dst_host_ srv_rerror_rate --> srv_rerror_rate_100

=== Run information ===

Evaluator:    weka.attributeSelection.InfoGainAttributeEval 
Search:       weka.attributeSelection.Ranker -T -1.7976931348623157E308 -N -1
Relation:     KDDTrain-weka.filters.unsupervised.attribute.Normalize-S1.0-T0.0-weka.filters.unsupervised.attribute.Discretize-B12-M-1.0-Rfirst-last-precision6
Instances:    125973
Attributes:   42
              duration
              protocol_type
              service
              flag
              src_bytes
              dst_bytes
              land
              wrong_fragment
              urgent
              hot
              num_failed_logins
              logged_in
              num_compromised
              root_shell
              su_attempted
              num_root
              num_file_creations
              num_shells
              num_access_files
              num_outbound_cmds
              is_host_login
              is_guest_login
              count
              srv_count
              serror_rate
              srv_serror_rate
              rerror_rate
              srv_rerror_rate
              same_srv_rate
              diff_srv_rate
              srv_diff_host_rate
              dst_host_count
              dst_host_srv_count
              dst_host_same_srv_rate
              dst_host_diff_srv_rate
              dst_host_same_src_port_rate
              dst_host_srv_diff_host_rate
              dst_host_serror_rate
              dst_host_srv_serror_rate
              dst_host_rerror_rate
              dst_host_srv_rerror_rate
              class
Evaluation mode:    evaluate on all training data



=== Attribute Selection on all input data ===

Search Method:
	Attribute ranking.

BIND 12

Attribute Evaluator (supervised, Class (nominal): 42 class):
	Information Gain Ranking Filter

Ranked attributes:
 0.6715649     3 service
 0.51938822    4 flag
 0.49133837   29 same_srv_rate
 0.46264173   33 dst_host_srv_count
 0.42131769   34 dst_host_same_srv_rate
 0.40475154   12 logged_in
 0.39010357   38 dst_host_serror_rate
 0.3871931    39 dst_host_srv_serror_rate
 0.37774343   25 serror_rate
 0.37680691   26 srv_serror_rate
 0.34988116   23 count
 0.16062108   32 dst_host_count
 0.08864437   31 srv_diff_host_rate
 0.0662392    41 dst_host_srv_rerror_rate
 0.06263911    2 protocol_type
 0.05191208   27 rerror_rate
 0.05096102   28 srv_rerror_rate
 0.05000081   40 dst_host_rerror_rate
 0.03956785   36 dst_host_same_src_port_rate
 0.03894094   24 srv_count
 0.02934163   30 diff_srv_rate
 0.02593891   35 dst_host_diff_srv_rate
 0.02556392   37 dst_host_srv_diff_host_rate
 0.00960996    8 wrong_fragment
 0.00540681    1 duration
 0.00216444   19 num_access_files
 0.00137056   10 hot
 0.00116837   22 is_guest_login
 0.00052957   15 su_attempted
 0.00043562   17 num_file_creations
 0.00032406   14 root_shell
 0.00013571   18 num_shells
 0.00012911   16 num_root
 0.00011326   11 num_failed_logins
 0.00010759   13 num_compromised
 0.00005256    5 src_bytes
 0.00003811    7 land
 0.00002628    6 dst_bytes
 0.0000089     9 urgent
 0.00000717   21 is_host_login
 0            20 num_outbound_cmds


BIND 3

=== Attribute Selection on all input data ===

Search Method:
	Attribute ranking.

Attribute Evaluator (supervised, Class (nominal): 42 class):
	Information Gain Ranking Filter

Ranked attributes:
 0.6715649     3 service
 0.51938822    4 flag
 0.46853425   29 same_srv_rate
 0.4184768    33 dst_host_srv_count
 0.40475154   12 logged_in
 0.38729949   34 dst_host_same_srv_rate
 0.37954447   39 dst_host_srv_serror_rate
 0.37373731   26 srv_serror_rate
 0.37013103   25 serror_rate
 0.36181956   38 dst_host_serror_rate
 0.17375199   23 count
 0.12038355   32 dst_host_count
 0.06263911    2 protocol_type
 0.05000185   41 dst_host_srv_rerror_rate
 0.04858996   27 rerror_rate
 0.04808819   40 dst_host_rerror_rate
 0.04808284   28 srv_rerror_rate
 0.02242257   24 srv_count
 0.01600674   37 dst_host_srv_diff_host_rate
 0.01531029   35 dst_host_diff_srv_rate
 0.01443348   36 dst_host_same_src_port_rate
 0.01179242   31 srv_diff_host_rate
 0.0077841     8 wrong_fragment
 0.00388943    1 duration
 0.00365365   30 diff_srv_rate
 0.00116837   22 is_guest_login
 0.00052957   15 su_attempted
 0.00032406   14 root_shell
 0.00020519   17 num_file_creations
 0.00015064   19 num_access_files
 0.00013571   18 num_shells
 0.00010053   11 num_failed_logins
 0.00003811    7 land
 0.00003504    5 src_bytes
 0.00003099   10 hot
 0.00000876    6 dst_bytes
 0.00000841    9 urgent
 0.00000717   21 is_host_login
 0.00000717   16 num_root
 0.00000717   13 num_compromised
 0            20 num_outbound_cmds
