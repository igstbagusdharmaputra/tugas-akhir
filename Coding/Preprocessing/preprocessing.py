import  csv
from collections import Counter
from prettytable import PrettyTable
with open('packetdata.csv') as csvfile:
    reader = csv.reader(csvfile)
    total_protocol = Counter()
    hasil = 0
    length = list()
    number = list()
    table = PrettyTable(["number", "protocol", "length"])
    for row in reader:
        number.append(row[0])
        length.append(row[5])
        table.add_row([row[0], row[4], row[5]])
        protokol = row[4]
        if protokol in "ICMPv6":
            total_protocol[protokol] += 1
        if protokol in "ICMP":
            total_protocol[protokol] += 1
        if protokol in "DNS":
            total_protocol[protokol] += 1
        if protokol in "DHCP":
            total_protocol[protokol] += 1
        if protokol in "HTTP":
            total_protocol[protokol] += 1
        if protokol in "OCSP":
            total_protocol[protokol] += 1
        if protokol in "TCP":
            total_protocol[protokol] += 1
        if protokol in "SSHv2":
            total_protocol[protokol] += 1
        if protokol in "NTP":
            total_protocol[protokol] += 1
        if protokol in "FTP":
            total_protocol[protokol] += 1
        if protokol in "ARP":
            total_protocol[protokol] += 1
        if protokol in "MDNS":
            total_protocol[protokol] += 1
        if protokol in "IGMPv3":
            total_protocol[protokol] += 1
        if protokol in "FTP-protokol":
            total_protocol[protokol] += 1
        if protokol in "TLSv1.2":
            total_protocol[protokol] += 1
        if protokol in "TLSv1.3":
            total_protocol[protokol] += 1
        if protokol in "SSLv2":
            total_protocol[protokol] += 1
print(" LENGTH ")
table_length= PrettyTable(["max","min"])
length.remove('Length')
length = results = [int(i) for i in length]
length_max = max(length)
length_min = min(length)
table_length.add_row([length_max,length_min])
print(table_length)
print(" PROTOKOL ")
table_protokol= PrettyTable(["Protocol","Total"])
for p in total_protocol:
    total=total_protocol[p]
    table_protokol.add_row([p,total])
print(table_protokol)
number.remove('No.')
for i in range (4982):
    print(table[i])