from random import  randrange

import  csv
def load_csv(filename):
    with open(filename) as csvfile:
        new_data = []
        reader = csv.reader(csvfile)
        for data in reader:
            new_data.append(data)
    return new_data
def cross_validation_split(dataset, n_folds):
	dataset_split = list()
	dataset_copy = list(dataset)
	fold_size = int(len(dataset) / n_folds)
	for i in range(n_folds):
		fold = list()
		while len(fold) < fold_size:
			index = randrange(len(dataset_copy))
			fold.append(dataset_copy.pop(index))
		dataset_split.append(fold)
	return dataset_split
filename ='seeds.csv'
dataset = load_csv(filename)
test = cross_validation_split(dataset,3)
print(test)

