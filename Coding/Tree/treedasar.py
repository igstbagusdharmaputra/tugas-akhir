class Node:
    def __init__(self,data):
        self.kiri = None
        self.kanan = None
        self.data = data

    def pohon(self,data):

        if self.data:
           if data < self.data:
               if self.kiri is None:
                   self.kiri = Node(data)
               else:
                   self.kiri.pohon(data)
           elif data > self.data:
               if self.kanan is None:
                   self.kanan = Node(data)
               else:
                    self.kanan.pohon(data)
        else:
            self.data = data

    def cetak(self):
        if self.kiri:
            self.kiri.cetak()
        print(self.data)
        if self.kanan:
            self.kanan.cetak()

root = Node(10)
root.pohon(13)
root.pohon(12)
root.pohon(11)
root.pohon(30)
root.pohon(1)
root.cetak()